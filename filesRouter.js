const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile } = require('./filesService.js');
const {editFile, deleteFile} = require("./filesService");

router.post('/', createFile);

router.get('/', getFiles);

router.get('/:filename', getFile)

router.put('/:filename', editFile)

router.delete('/:filename', deleteFile)

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router
};


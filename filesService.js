// requires...
const fs = require('fs')
const path = require('path')

// constants...

 function createFile(req, res, next) {

  // Your code to create the file.
  // let allPasswords = new Map()
  //    fs.access(path.join(__dirname, "password.json"), fs.F_OK, (err) => {
  //   if (err) {
  //     fs.writeFile(path.join(__dirname, "password.json"),"", (err) => {
  //       if (err) {
  //         throw err
  //       }
  //     })
  //   }
  // })
  // let content = '';
  //     fs.readFile(path.join(__dirname, "password.json"), 'utf8',(err, data)=>{
  //   if (err) {
  //     throw err
  //   }
  //   content = data
  // })
  //
  //
  // if (content) {
  //   allPasswords = new Map(Object.entries(JSON.parse(content)))
  // }

  // if (!allPasswords.get(req.body.filename)) {
  //   allPasswords.set(req.body.filename, req.body.password)
  //
  // }

  // fs.writeFile(path.join(__dirname, "password.json"), JSON.stringify(Object.fromEntries(allPasswords)), (err)=>{
  //   if (err) {
  //     throw err
  //   }
  // })

   if(!req.body.content) {
     res.status(400).send({
       "message": "Please specify 'content' parameter"
     })
     return
   }
   if(!req.body.filename) {
     res.status(400).send({
       "message": "Please specify 'content' parameter"
     })
     return
   }
   let ext = path.extname(req.body.filename).split('.')[1]

  let arrOfAllowedExts = ['log', 'txt', 'json', 'yaml', 'xml']

  if (arrOfAllowedExts.includes(ext)) {
    fs.writeFile(path.join(__dirname, 'files', req.body.filename), req.body.content, (err) => {
      if (err) {
        throw err
      }
      console.log('file created')
    })
    res.status(200).send({"message": "File created successfully"})

  } else {
    console.error('this extention of file is not supported')
    res.status(400).send({"message": "File extention is not supported"});
  }

}

function getFiles(req, res, next) {
  // Your code to get all files.

  fs.readdir(path.join(__dirname, 'files', '/'), (err, files) => {
    files.forEach(file => {
      console.log(file)
    });
    res.status(200).send({
      "message": "Success",
      "files": files,
    });
  });


}

const getFile = (req, res, next) => {
  let ext = path.extname(req.params.filename).split('.')[1]
  console.log(ext)
  let arrOfAllowedExts = ['log', 'txt', 'json', 'yaml', 'xml']


  // fs.access(path.join(__dirname, "password.json"), fs.F_OK, (err) => {
  //   if (err) {
  //     fs.writeFile(path.join(__dirname, "password.json"),"", (err) => {
  //       if (err) {
  //         throw err
  //       }
  //       console.log('json created')
  //     })
  //   }
  // })

  // if (fs.readFileSync(path.join(__dirname, "password.json",), 'utf8')) {
  //   let passwordFromUrl = req.query.password
  //   let content = fs.readFileSync(path.join(__dirname, "password.json",), 'utf8')
  //   let allPasswords = new Map(Object.entries(JSON.parse(content)))
  //   let passwordCorrect = allPasswords.get(req.params.filename)
  if (arrOfAllowedExts.includes(ext)){
    fs.access(path.join(__dirname, 'files', req.params.filename), (err) => {
      if (err) {
        console.error('nofile' + err)
        res.status(400).send({
          "message": `No file with ${req.params.filename} filename found`
        })
        return
      }
    })
    fs.readFile(path.join(__dirname, 'files', req.params.filename), 'utf8', (err, data) => {

      if (err) {
        console.error('here is error' + err);
        return;
      }
      // if (passwordFromUrl === passwordCorrect) {
      res.status(200).send({
        "message": "Success",
        "filename": req.params.filename,
        "content": data,
        "extension": path.extname(req.params.filename).split('.')[1],
        "uploadedDate": fs.statSync(path.join(__dirname, 'files', req.params.filename)).birthtime
      });
      // } else {
      //   res.status(403).send("wrong password")
      // }

    });
  } else {
    res.status(400).send({
      "message": "file extentin is not supported"
    })
  }



  // } else {
  //   res.status(403).send("no password")
  // }





  // Your code to get all files.


}

// Other functions - editFile, deleteFile
const editFile = (req, res, next) => {
  fs.writeFile(path.join(__dirname, 'files', req.params.filename), req.body.content,
      {
        encoding: "utf8",
        flag: "a",
      },
      (err) => {
        if (err)
          console.log(err);
        else {
          console.log("File written successfully\n");
          console.log("The written has the following contents:" + req.body.content);
          res.status(200).send({
            "message": "Success",
            "filename": req.params.filename,
            "content": req.body.content,
          });

        }
      });
}

const deleteFile = (req, res, next) => {
  fs.unlink(path.join(__dirname, 'files', req.params.filename), (err) => {
    if (err) {
      console.error(err)
      return
    } else {
      res.status(200).send({
        "message": "Success deleted",
        "filename": req.params.filename,
      });

      //file removed

    }
  })
}
// path.extName('file.txt') ---> '.txt'
// fs.writeFile ({ flag: 'a' }) ---> adds content to the file

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
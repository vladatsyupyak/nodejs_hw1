const fs = require('fs');
const express = require('express');
const morgan = require('morgan')
const app = express();
const path = require('path')



const { filesRouter } = require('./filesRouter.js');
const {createFile} = require("./filesService");
const {getFiles} = require("./filesService")
const {getFile} = require("./filesService")
// var accessLogStream = fs.createWriteStream(path.join(__dirname, 'access.log'), {flags: 'a'})
app.use(express.json());
app.use(morgan('tiny'));
//
app.use('/api/files', filesRouter);
// app.use(morgan('tiny', {stream: accessLogStream}))

const start = async () => {
  try {
    if (!fs.existsSync('files')) {
      fs.mkdirSync('files');
    }
    app.listen(8080, ()=> console.log('test'));
    console.log('hi')

  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
}
app.use(createFile)
app.use(getFiles)
app.use(getFile)



start();





//ERROR HANDLER
app.use(errorHandler)

function errorHandler (err, req, res, next) {
  console.error('err')
  console.log(err)
  res.status(500).send({'message': 'Server error'});
}



